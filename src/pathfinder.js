let gamemap,
    oldGamemap,
    isSearchPaused = true,
    pathfinder,
    stepsAllowed = 0,
    runPauseButton,
    mapGraphic = null,
    nodeDistance = 1,
    stateText,
    state,
    path,
    oldObstacles = [],
    isHeuristicMenuEnabled = false,
    pathLength = 0,
    pathfinderTestsMode = false

const initaliseSearchRoutine = (columns, rows, isNewSearchMapNeeded) => {
    mapGraphic = null

    gamemap = new SearchMap(columns, rows, 10, 10, 640, 640, percentWalls)

    if (!isNewSearchMapNeeded) {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                gamemap.grid[i][j].isObstacle = oldObstacles.shift()
            }
        }
    }

    oldObstacles = []
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            oldObstacles.push(gamemap.grid[i][j].isObstacle)
        }
    }

    start = gamemap.grid[0][0]
    goal = gamemap.grid[columns - 1][rows - 1]
    start.isObstacle = false
    goal.isObstacle = false

    pathfinder = (_ => {
        if (!isBidirectional) {
            switch (findAlgorithm) {
                case 'dijkstra':
                    return new DijkstraSearch(gamemap, start, goal, isDiagonalAllowed)
                case 'astar':
                    return new AStarSearch(gamemap, start, goal, heuristicType, isDiagonalAllowed)
                case 'breadthfirst':
                    return new BreadthFirstSearch(gamemap, start, goal)
                case 'depthfirst':
                    return new DepthFirstSearch(gamemap, start, goal)
                case 'bestfirst':
                    return new BestFirstSearch(
                        gamemap,
                        start,
                        goal,
                        heuristicType,
                        isDiagonalAllowed
                    )
            }
        } else {
            switch (findAlgorithm) {
                case 'dijkstra':
                    return new BiDijkstraSearch(gamemap, start, goal, isDiagonalAllowed)
                case 'astar':
                    return new BiAStarSearch(gamemap, start, goal, heuristicType, isDiagonalAllowed)
                case 'breadthfirst':
                    return new BiBreadthFirstSearch(gamemap, start, goal)
                case 'depthfirst':
                    return new BiDepthFirstSearch(gamemap, start, goal)
                case 'bestfirst':
                    return new BiBestFirstSearch(
                        gamemap,
                        start,
                        goal,
                        heuristicType,
                        isDiagonalAllowed
                    )
            }
        }
    })()
}

const drawMap = _ => {
    if (!mapGraphic) {
        for (let i = 0; i < gamemap.columns; i++) {
            for (let j = 0; j < gamemap.rows; j++) {
                if (gamemap.grid[i][j].isObstacle) {
                    gamemap.grid[i][j].show(color(255))
                }
            }
        }
        mapGraphic = get(gamemap.x, gamemap.y, gamemap.w, gamemap.h)
    }

    image(mapGraphic, gamemap.x, gamemap.y)
}

setup = _ => {
    setSearchState(SEARCH_IDLE)

    if (pathfinderTestsMode) updateSettings(0)

    initaliseSearchRoutine(columns, rows, true)

    createCanvas(windowWidth, windowHeight)

    createGUI()

    if (pathfinderTestsMode) pauseStartButtonAction()
}

draw = _ => {
    searchStep()

    renderGUI()

    startTimer()

    drawMap()

    let infoNode = null

    for (let i = 0; i < pathfinder.closedList.length; i++) {
        let node = pathfinder.closedList[i]
        node.show(color(255, 0, 0, 50))
        if (
            mouseX > node.x &&
            mouseX < node.x + node.width &&
            mouseY > node.y &&
            mouseY < node.y + node.height
        ) {
            infoNode = node
        }
    }

    for (let i = 0; i < pathfinder.openList.length; i++) {
        let node = pathfinder.openList[i]
        node.show(color(0, 255, 0, 80))
        if (
            mouseX > node.x &&
            mouseX < node.x + node.width &&
            mouseY > node.y &&
            mouseY < node.y + node.height
        ) {
            infoNode = node
        }
    }

    recordTimer('mapDrawTime')

    fill(0)
    if (infoNode != null) {
        text('Selected node info:', 660, 515)
        text('f = ' + infoNode.f.toFixed(4), 660, 530)
        text('g = ' + infoNode.g.toFixed(4), 660, 545)
        text('h = ' + infoNode.h.toFixed(4), 660, 560)
    }

    if (getSearchState() === SEARCH_SUCCEED) {
        drawPath(getSolutionPath())

        if (pathfinderTestsMode) pathfinderTest()
    }
}
