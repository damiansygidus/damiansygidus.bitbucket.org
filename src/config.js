let isDiagonalAllowed = true,
    isBidirectional = true,
    isCrossCornersAllowed = false,
    columns = 64,
    rows = 64,
    findAlgorithm = 'astar',
    // 'astar'
    // 'dijkstra'
    // 'breadthfirst'
    // 'depthfirst'
    // 'bestfirst'
    heuristicType = 'octile',
    // 'manhattan'
    // 'octile'
    // 'euclidean'
    // 'chebyshev'
    percentWalls = isDiagonalAllowed ? (isCrossCornersAllowed ? 0.4 : 0.3) : 0.2
