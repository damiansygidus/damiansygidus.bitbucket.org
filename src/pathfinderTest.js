let testsCount = 0,
    testScenarioNumber = 1

const testScenarios = [
    // One way
    [true, false, false, 'astar', 'octile'],
    [true, false, false, 'astar', 'euclidean'],
    [true, false, false, 'astar', 'chebyshev'],
    [true, false, false, 'bestfirst', 'octile'],
    [true, false, false, 'bestfirst', 'euclidean'],
    [true, false, false, 'bestfirst', 'chebyshev'],
    [true, false, false, 'dijkstra', ''],
    [true, false, false, 'breadthfirst', ''],
    [true, false, false, 'depthfirst', ''],

    // Bidirectional
    [true, true, false, 'astar', 'octile'],
    [true, true, false, 'astar', 'euclidean'],
    [true, true, false, 'astar', 'chebyshev'],
    [true, true, false, 'bestfirst', 'octile'],
    [true, true, false, 'bestfirst', 'euclidean'],
    [true, true, false, 'bestfirst', 'chebyshev'],
    [true, true, false, 'dijkstra', ''],
    [true, true, false, 'breadthfirst', ''],
    [true, true, false, 'depthfirst', ''],

    // Diagonal movement disabled, one way
    [false, false, false, 'astar', 'manhattan'],
    [false, false, false, 'bestfirst', 'manhattan'],
    [false, false, false, 'dijkstra', ''],
    [false, false, false, 'breadthfirst', ''],
    [false, false, false, 'depthfirst', ''],

    // Diagonal movement disabled, bidirectional
    [false, true, false, 'astar', 'manhattan'],
    [false, true, false, 'bestfirst', 'manhattan'],
    [false, true, false, 'dijkstra', ''],
    [false, true, false, 'breadthfirst', ''],
    [false, true, false, 'depthfirst', ''],
]

var updateSettings = testScenarioNumber => {
    isDiagonalAllowed = testScenarios[testScenarioNumber][0]
    isBidirectional = testScenarios[testScenarioNumber][1]
    isCrossCornersAllowed = testScenarios[testScenarioNumber][2]
    findAlgorithm = testScenarios[testScenarioNumber][3]
    heuristicType = testScenarios[testScenarioNumber][4]
}

var pathfinderTest = _ => {
    testScenarioNumber == testScenarios.length
        ? restartButtonAction()
        : (_ => {
              updateSettings(testScenarioNumber++)
              clearButtonAction()
              pauseStartButtonAction()
          })()
}
