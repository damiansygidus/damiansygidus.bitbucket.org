heuristics = {
    manhattan: (x, y, D) => D * (x + y),

    euclidean: (x, y, D) => D * Math.sqrt(x * x + y * y),

    octile: (x, y) => {
        let E = Math.SQRT2 - 1
        return x < y ? E * x + y : E * y + x
    },

    chebyshev: (x, y) => Math.max(x, y)
}

const heuristicFunc = (x, y, D, type) => {
    dx = abs(x.i - y.i)
    dy = abs(x.j - y.j)
    
    switch (type) {
        case 'manhattan':
            return heuristics.manhattan(dx, dy, D)
        case 'octile':
            return heuristics.octile(dx, dy)
        case 'euclidean':
            return heuristics.euclidean(dx, dy, D)
        case 'chebyshev':
            return heuristics.chebyshev(dx, dy)
        default:
            return
    }
}
