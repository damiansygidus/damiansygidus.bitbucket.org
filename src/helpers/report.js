let timer
let timings = {}

const clearTimings = _ => {
    timings = {}
}

const startTimer = _ => {
    timer = millis()
}

const recordTimer = n => {
    if (!timings[n]) {
        timings[n] = {
            sum: millis() - timer,
            count: 1
        }
    } else {
        timings[n].sum = timings[n].sum + millis() - timer
        timings[n].count = timings[n].count + 1
    }
}

// const logTimings = _ => {
//     for (let prop in timings) {
//         if (timings.hasOwnProperty(prop)) {
//             console.log(prop + ' = ' + (timings[prop].sum / timings[prop].count).toString() + ' ms')
//         }
//     }
// }

const logReport = _ => {
    let report = 'Test finished with status: ' + stateLabels[state]

    let heuristicTypeReport =
        '\nheuristicType: ' +
        (_ => {
            if (findAlgorithm === 'astar' || findAlgorithm === 'bestfirst') {
                return isDiagonalAllowed ? heuristicType.toString() : 'manhattan'
            } else {
                return 'none'
            }
        })()

    let settingsReport = '\n\nSettings:' +
        '\npathfindingAlgorithm = ' +
        findAlgorithm.toString() +
        heuristicTypeReport +
        '\nisDiagonalAllowed = ' +
        isDiagonalAllowed.toString() +
        '\nisCrossCornersAllowed = ' +
        isCrossCornersAllowed.toString() +
        '\nisBidirectional = ' +
        isBidirectional.toString() +
        '\npercentWalls = ' +
        percentWalls.toString() +
        '\ncolumns = ' +
        columns.toString() +
        '\nrows = ' +
        rows.toString()

    let timingsReport = '\n\nTimings:'
    
    for (let timerName in timings) {
        if (timings.hasOwnProperty(timerName)) {
            timingsReport +=
                '\n' +
                timerName +
                'Average = ' +
                (timings[timerName].sum / timings[timerName].count).toString() +
                ' ms' +
                '\n' +
                timerName +
                'Count = ' +
                timings[timerName].count.toString() +
                '\n' +
                timerName +
                'Total = ' +
                timings[timerName].sum.toString() +
                ' ms'
        }
    }

    let pathReport = '\n\nPath:' + 
        '\npathLength = ' + (getSolutionPath().length - 1) +
        '\npathDistance = ' + calculateSolutionDistance(getSolutionPath())

    report += settingsReport + timingsReport + pathReport + '\n'
    console.log(report)
}
