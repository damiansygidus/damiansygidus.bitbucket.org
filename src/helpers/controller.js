const SEARCH_IDLE = 0,
    SEARCH_RUNNING = 1,
    SEARCH_PAUSED = 2,
    SEARCH_SUCCEED = 3,
    SEARCH_FAILED = 4,
    SEARCH_ERROR = 5

const stateLabels = [
    'Idle mode',
    'Searching for solution...',
    'Search paused',
    'Solution found!',
    'No avaliable solution',
    'Error!'
]

const setSearchState = newState => {
    stateText = stateLabels[state]
    state = newState
}

const getSearchState = _ => state

const searchStep = _ => {
    if (!isSearchPaused || stepsAllowed > 0) {
        startTimer()
        let result = pathfinder.step()
        recordTimer('algorithmTime')
        stepsAllowed--

        switch (result) {
            case -1:
                setSearchState(SEARCH_FAILED)
                logReport()
                switchPause(true)
                break
            case 1:
                setSearchState(SEARCH_SUCCEED)
                logReport()
                switchPause(true)
                break
            case 0:
                setSearchState(SEARCH_RUNNING)
                break
        }
    }
}

const stepButtonAction = _ => {
    if (getSearchState() != SEARCH_SUCCEED) {
        switchPause(true)
        stepsAllowed = 1
    }
}

const switchPause = pause => {
    isSearchPaused = pause
    pauseStartButton.label = isSearchPaused ? 'Start' : 'Pause'
}

const pauseStartButtonAction = _ => {
    if (getSearchState() != SEARCH_SUCCEED) {
        switchPause(!isSearchPaused)
    }
}

const restartButtonAction = _ => {
    setSearchState(SEARCH_IDLE)
    clearTimings()
    initaliseSearchRoutine(columns, rows, true)
    switchPause(true)
}

const clearButtonAction = _ => {
    setSearchState(SEARCH_IDLE)
    clearTimings()
    initaliseSearchRoutine(columns, rows, false)
    switchPause(true)
}

const toggleBidirectional = _ => {
    isBidirectional = !isBidirectional
    clearButtonAction()
}
const toggleDiagonals = _ => {
    isDiagonalAllowed = !isDiagonalAllowed
    clearButtonAction()
}

const toggleCornerCrossing = _ => {
    isCrossCornersAllowed = !isCrossCornersAllowed
    clearButtonAction()
}

const setDijkstraFinder = _ => {
    findAlgorithm = 'dijkstra'
    clearButtonAction()
}

const setAstarFinder = _ => {
    findAlgorithm = 'astar'
    clearButtonAction()
}

const setBestFirstFinder = _ => {
    findAlgorithm = 'bestfirst'
    clearButtonAction()
}

const setBreadthFirstFinder = _ => {
    findAlgorithm = 'breadthfirst'
    clearButtonAction()
}

const setDepthFirstFinder = _ => {
    findAlgorithm = 'depthfirst'
    clearButtonAction()
}

const setOctileHeuristic = _ => {
    heuristicType = 'octile'
    clearButtonAction()
}

const setEuclideanHeuristic = _ => {
    heuristicType = 'euclidean'
    clearButtonAction()
}

const setChebyshevHeuristic = _ => {
    heuristicType = 'chebyshev'
    clearButtonAction()
}

const setManhattanHeuristic = _ => {
    heuristicType = 'manhattan'
    clearButtonAction()
}

var mouseClicked = _ => {
    for (let i = 0; i < GUIElements.length; i++) {
        GUIElements[i].mouseClick(mouseX, mouseY)
    }
    if (isDiagonalAllowed) {
        for (let i = 0; i < DiagonalHeuristicsSelectOptions.length; i++) {
            DiagonalHeuristicsSelectOptions[i].mouseClick(mouseX, mouseY)
        }
    }
}

const disableAllSelectOptions = optionsArray => {
    for (let i = 0; i < optionsArray.length; i++) {
        optionsArray[i].isSet = false
    }
}
