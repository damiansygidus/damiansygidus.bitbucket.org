function BiAStarSearch(grid, start, goal, heuristicType, isDiagonalAllowed) {
    this.grid = grid
    this.startLastCheckedNode = start
    this.goalLastCheckedNode = goal

    this.openList = []
    this.startOpenList = []
    this.goalOpenList = []

    this.startOpenList.push(start)
    this.goalOpenList.push(goal)

    this.closedList = []
    this.start = start
    this.goal = goal

    this.isDiagonalAllowed = isDiagonalAllowed
    this.heuristicType = heuristicType

    this.visualDistance = (a, b) => dist(a.i, a.j, b.i, b.j)

    this.heuristic = (a, b) =>
        isDiagonalAllowed
            ? heuristicFunc(a, b, nodeDistance, heuristicType)
            : heuristicFunc(a, b, nodeDistance, 'manhattan')

    this.removeFromArray = (array, element) => {
        array.splice(
            array.findIndex(array_element => {
                return array_element == element
            }),
            1
        )
    }

    let BY_START = 1,
        BY_GOAL = 2,
        current,
        neighbors

    this.step = _ => {
        if (this.startOpenList.length && this.goalOpenList.length) {
            
            let startBestIndex = 0

            for (let i = 1; i < this.startOpenList.length; i++) {
                if (this.startOpenList[i].f < this.startOpenList[startBestIndex].f) {
                    startBestIndex = i
                }

                if (this.startOpenList[i].f == this.startOpenList[startBestIndex].f) {
                    if (this.startOpenList[i].g > this.startOpenList[startBestIndex].g) {
                        startBestIndex = i
                    }

                    if (!this.isDiagonalAllowed) {
                        if (
                            this.startOpenList[i].g == this.startOpenList[startBestIndex].g &&
                            this.startOpenList[i].vh < this.startOpenList[startBestIndex].vh
                        ) {
                            startBestIndex = i
                        }
                    }
                }
            }

            current = this.startOpenList[startBestIndex]
            this.startLastCheckedNode = current

            this.removeFromArray(this.startOpenList, current)
            this.closedList.push(current)

            neighbors = current.getNeighbors()

            for (let i = 0; i < neighbors.length; i++) {
                let neighbor = neighbors[i]

                if (!this.closedList.includes(neighbor)) {
                    if (neighbor.visited) {
                        if (neighbor.visitedBy === BY_GOAL) {
                            this.goalLastCheckedNode = neighbor
                            return 1
                        }
                        continue
                    }

                    let tentativeG =
                        current.g +
                        (neighbor.i - current.i === 0 || neighbor.j - current.j === 0
                            ? 1
                            : Math.SQRT2)

                    if (!this.startOpenList.includes(neighbor)) {
                        this.startOpenList.push(neighbor)
                    } else if (tentativeG >= neighbor.g) {
                        continue
                    }

                    neighbor.g = tentativeG
                    neighbor.h = this.heuristic(neighbor, goal)
                    if (!isDiagonalAllowed) {
                        neighbor.vh = this.visualDistance(neighbor, goal)
                    }
                    neighbor.f = neighbor.g + neighbor.h
                    neighbor.previous = current

                    neighbor.visited = true
                    neighbor.visitedBy = BY_START
                }
            }
            
            let goalBestIndex = 0
            
            for (let i = 1; i < this.goalOpenList.length; i++) {
                if (this.goalOpenList[i].f < this.goalOpenList[goalBestIndex].f) {
                    goalBestIndex = i
                }

                if (this.goalOpenList[i].f == this.goalOpenList[goalBestIndex].f) {
                    if (this.goalOpenList[i].g > this.goalOpenList[goalBestIndex].g) {
                        goalBestIndex = i
                    }

                    if (!this.isDiagonalAllowed) {
                        if (
                            this.goalOpenList[i].g == this.goalOpenList[goalBestIndex].g &&
                            this.goalOpenList[i].vh < this.goalOpenList[goalBestIndex].vh
                        ) {
                            goalBestIndex = i
                        }
                    }
                }
            }
            current = this.goalOpenList[goalBestIndex]
            this.goalLastCheckedNode = current

            this.removeFromArray(this.goalOpenList, current)
            this.closedList.push(current)

            neighbors = current.getNeighbors()

            for (let i = 0; i < neighbors.length; i++) {
                let neighbor = neighbors[i]

                if (!this.closedList.includes(neighbor)) {
                    if (neighbor.visited) {
                        if (neighbor.visitedBy === BY_START) {
                            this.startLastCheckedNode = neighbor
                            return 1
                        }
                        continue
                    }

                    let tentativeG =
                        current.g +
                        (neighbor.i - current.i === 0 || neighbor.j - current.j === 0
                            ? 1
                            : Math.SQRT2)

                    if (!this.goalOpenList.includes(neighbor)) {
                        this.goalOpenList.push(neighbor)
                    } else if (tentativeG >= neighbor.g) {
                        continue
                    }

                    neighbor.g = tentativeG
                    neighbor.h = this.heuristic(neighbor, start)
                    if (!isDiagonalAllowed) {
                        neighbor.vh = this.visualDistance(neighbor, start)
                    }
                    neighbor.f = neighbor.g + neighbor.h
                    neighbor.previous = current

                    neighbor.visited = true
                    neighbor.visitedBy = BY_GOAL
                }
            }

            this.openList = this.startOpenList.concat(this.goalOpenList)
            return 0
        } else {
            return -1
        }
    }
}
