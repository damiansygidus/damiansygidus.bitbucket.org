function AStarSearch(grid, start, goal, heuristicType, isDiagonalAllowed) {
    this.grid = grid
    this.lastCheckedNode = start
    
    this.openList = []

    this.openList.push(start)
    this.closedList = []
    
    this.start = start
    this.goal = goal
    
    this.isDiagonalAllowed = isDiagonalAllowed
    this.heuristicType = heuristicType

    this.visualDistance = (a, b) => dist(a.i, a.j, b.i, b.j)

    this.heuristic = (a, b) =>
        isDiagonalAllowed
            ? heuristicFunc(a, b, nodeDistance, heuristicType)
            : heuristicFunc(a, b, nodeDistance, 'manhattan')

    this.removeFromArray = (array, element) => {
        array.splice(
            array.findIndex(array_element => {
                return array_element == element
            }),
            1
        )
    }

    this.step = _ => {
        if (this.openList.length) {
            let bestIndex = 0

            for (let i = 1; i < this.openList.length; i++) {
                if (this.openList[i].f < this.openList[bestIndex].f) {
                    bestIndex = i
                }

                if (this.openList[i].f == this.openList[bestIndex].f) {
                    if (this.openList[i].g > this.openList[bestIndex].g) {
                        bestIndex = i
                    }

                    if (!this.isDiagonalAllowed) {
                        if (
                            this.openList[i].g == this.openList[bestIndex].g &&
                            this.openList[i].vh < this.openList[bestIndex].vh
                        ) {
                            bestIndex = i
                        }
                    }
                }
            }

            var current = this.openList[bestIndex]
            this.lastCheckedNode = current

            if (current === this.goal) {
                return 1
            }

            this.removeFromArray(this.openList, current)
            this.closedList.push(current)

            let neighbors = current.getNeighbors()

            for (let i = 0; i < neighbors.length; i++) {
                let neighbor = neighbors[i]

                if (!this.closedList.includes(neighbor)) {
                    let tentativeG =
                        current.g +
                        (neighbor.i - current.i === 0 || neighbor.j - current.j === 0
                            ? 1
                            : Math.SQRT2)

                    if (!this.openList.includes(neighbor)) {
                        this.openList.push(neighbor)
                    } else if (tentativeG >= neighbor.g) {
                        continue
                    }

                    neighbor.g = tentativeG
                    neighbor.h = this.heuristic(neighbor, goal)
                    if (!isDiagonalAllowed) {
                        neighbor.vh = this.visualDistance(neighbor, goal)
                    }
                    neighbor.f = neighbor.g + neighbor.h
                    neighbor.previous = current
                }
            }

            return 0
        } else {
            return -1
        }
    }
}
