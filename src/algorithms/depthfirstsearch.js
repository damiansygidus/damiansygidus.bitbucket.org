function DepthFirstSearch(grid, start, goal) {
  this.grid = grid
  this.lastCheckedNode = start
  this.openList = []

  this.openList.push(start)
  this.closedList = []
  this.start = start
  this.goal = goal

  this.step = _ => {
    if (this.openList.length > 0) {
      var current = this.openList.pop()
      this.lastCheckedNode = current

      this.closedList.push(current)

      if (current === this.goal) {
        return 1
      }

      let neighbors = current.getNeighbors()
      for (i = 0; i < neighbors.length; ++i) {
        neighbor = neighbors[i]
 
        if (
          this.openList.includes(neighbor) ||
          this.closedList.includes(neighbor)
        ) {
          continue
        }

        this.openList.push(neighbor)
        neighbor.previous = current
      }
      return 0
    } else {
      return -1
    }
  }
}
