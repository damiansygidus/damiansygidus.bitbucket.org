function DijkstraSearch(grid, start, goal, isDiagonalAllowed) {
    AStarSearch.call(this, grid, start, goal, '', isDiagonalAllowed)
    this.visualDistance = (a, b) => 0
    this.heuristic = (a, b) => 0
}
