function BestFirstSearch(grid, start, goal, heuristicType, isDiagonalAllowed) {
    AStarSearch.call(this, grid, start, goal, '', isDiagonalAllowed)
    this.heuristic = (a, b) =>
        isDiagonalAllowed
            ? heuristicFunc(a, b, nodeDistance, heuristicType) * 100000
            : heuristicFunc(a, b, nodeDistance, 'manhattan') * 100000
}
