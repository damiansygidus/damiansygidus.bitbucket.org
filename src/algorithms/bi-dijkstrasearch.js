function BiDijkstraSearch(grid, start, goal, isDiagonalAllowed) {
    BiAStarSearch.call(this, grid, start, goal, '', isDiagonalAllowed)
    this.visualDistance = (a, b) => 0
    this.heuristic = (a, b) => 0
}
