function BiDepthFirstSearch(grid, start, goal) {
    this.grid = grid
    this.startLastCheckedNode = start
    this.goalLastCheckedNode = goal

    this.openList = []
    this.closedList = []
    this.startOpenList = []
    this.goalOpenList = []

    this.startOpenList.push(start)
    this.goalOpenList.push(goal)

    this.closedList = []

    this.start = start
    this.goal = goal

    this.startNodeBy
    this.goalNodeBy

    let BY_START = 0,
        BY_GOAL = 1,
        current,
        neighbors

    this.step = _ => {
        if (this.startOpenList.length && this.goalOpenList.length) {
            current = this.startOpenList.pop()
            this.startLastCheckedNode = current
            this.closedList.push(current)

            neighbors = current.getNeighbors()

            for (i = 0, l = neighbors.length; i < l; ++i) {
                neighbor = neighbors[i]

                if (this.closedList.includes(neighbor)) {
                    continue
                }

                if (neighbor.visited) {
                    if (neighbor.visitedBy === BY_GOAL) {
                        this.goalLastCheckedNode = neighbor
                        return 1
                    }
                    continue
                }
                this.startOpenList.push(neighbor)
                neighbor.previous = current
                neighbor.visited = true
                neighbor.visitedBy = BY_START
            }

            current = this.goalOpenList.pop()
            this.goalLastCheckedNode = current
            this.closedList.push(current)

            neighbors = current.getNeighbors()
            for (i = 0, l = neighbors.length; i < l; ++i) {
                neighbor = neighbors[i]

                if (this.closedList.includes(neighbor)) {
                    continue
                }

                if (neighbor.visited) {
                    if (neighbor.visitedBy === BY_START) {
                        this.startLastCheckedNode = neighbor
                        return 1
                    }
                    continue
                }
                this.goalOpenList.push(neighbor)
                neighbor.previous = current
                neighbor.visited = true
                neighbor.visitedBy = BY_GOAL
            }

            this.openList = this.startOpenList.concat(this.goalOpenList)

            return 0
        } else {
            return -1
        }
    }
}
