class SelectButton {
    constructor(group, label, x, y, isSet, callback) {
        this.label = label
        this.x = x
        this.y = y
        this.isSet = isSet
        this.callback = callback

        this.show = _ => {
            strokeWeight(1)
            stroke(0)
            noFill()
            ellipse(this.x + 10, this.y + 10, 20, 20)
            if (this.isSet) {
                fill(0)
                ellipse(this.x + 10, this.y + 10, 3, 3)
            }
            fill(0)
            noStroke()
            text(label, this.x + 25, this.y + 15)
        }

        this.mouseClick = (x, y) => {
            if (x > this.x && x <= this.x + 20 && y > this.y && y <= this.y + 20) {
                if (!this.isSet) {
                    disableAllSelectOptions(group)
                    this.isSet = true
                    if (this.callback != null) this.callback(this)
                }
            }
        }
    }
}

class ToggleButton {
    constructor(label, x, y, isSet, callback) {
        this.label = label
        this.x = x
        this.y = y
        this.isSet = isSet
        this.callback = callback

        this.show = _ => {
            strokeWeight(1)
            stroke(0)
            noFill()
            ellipse(this.x + 10, this.y + 10, 20, 20)
            if (this.isSet) {
                fill(0)
                ellipse(this.x + 10, this.y + 10, 3, 3)
            }
            fill(0)
            noStroke()
            text(label, this.x + 25, this.y + 15)
        }

        this.mouseClick = (x, y) => {
            if (x > this.x && x <= this.x + 20 && y > this.y && y <= this.y + 20) {
                this.isSet = !this.isSet
                if (this.callback != null) this.callback(this)
            }
        }
    }
}

class Button {
    constructor(label, x, y, w, h, callback) {
        this.label = label
        this.x = x
        this.y = y
        this.w = w
        this.h = h
        this.callback = callback

        this.show = _ => {
            stroke(0)
            strokeWeight(1)
            noFill()
            rect(this.x, this.y, this.w, this.h)
            fill(0)
            noStroke()
            text(this.label, this.x + 5, this.y + 5, this.w - 10, this.h - 10)
        }

        this.mouseClick = (x, y) => {
            if (
                this.callback != null &&
                x > this.x &&
                x <= this.x + this.w &&
                y > this.y &&
                y <= this.y + this.h
            ) {
                this.callback(this)
            }
        }
    }
}

class Label {
    constructor(label, x, y) {
        this.label = label
        this.x = x
        this.y = y

        this.show = _ => {
            fill(0)
            text(this.label, this.x, this.y)
        }
    }
}

let GUIElements = [],
    AlgorithmSelectOptions = [],
    DiagonalHeuristicsSelectOptions = [],
    pauseStartButton,
    stepButton,
    restartButton,
    clearButton,
    isDiagonalAllowedRadio,
    isBidirectionalRadio,
    isCrossCornersAllowedRatio,
    dijkstraSelectButton,
    aStarSelectButton,
    bestFirstSelectButton,
    breadthFirstSelectButton,
    depthirstSelectButton,
    octileHeuristicSelectButton,
    euclideanHeuristicSelectButton,
    chebyshevHeuristicSelectButton

const createGUI = _ => {
    pauseStartButton = new Button('Start', 660, 30, 50, 30, pauseStartButtonAction)
    stepButton = new Button('Step', 720, 30, 50, 30, stepButtonAction)
    clearButton = new Button('Clear', 780, 30, 50, 30, clearButtonAction)
    restartButton = new Button('Restart', 840, 30, 50, 30, restartButtonAction)

    aStarSelectButton = new SelectButton(
        AlgorithmSelectOptions,
        'A* Algorithm',
        680,
        95,
        findAlgorithm === 'astar',
        setAstarFinder
    )
    dijkstraSelectButton = new SelectButton(
        AlgorithmSelectOptions,
        'Dijkstra Algorithm',
        680,
        125,
        findAlgorithm === 'dijkstra',
        setDijkstraFinder
    )
    bestFirstSelectButton = new SelectButton(
        AlgorithmSelectOptions,
        'Best First Algorithm',
        680,
        155,
        findAlgorithm === 'bestfirst',
        setBestFirstFinder
    )
    breadthFirstSelectButton = new SelectButton(
        AlgorithmSelectOptions,
        'Breadth First Algorithm',
        680,
        185,
        findAlgorithm === 'breadthfirst',
        setBreadthFirstFinder
    )
    depthirstSelectButton = new SelectButton(
        AlgorithmSelectOptions,
        'Depth First Algorithm',
        680,
        215,
        findAlgorithm === 'depthfirst',
        setDepthFirstFinder
    )
    isBidirectionalRadio = new ToggleButton(
        'Bidirectional Search Mode',
        680,
        280,
        isBidirectional,
        toggleBidirectional
    )
    isDiagonalAllowedRadio = new ToggleButton(
        'Allow Diagonal Movement',
        680,
        310,
        isDiagonalAllowed,
        toggleDiagonals
    )
    isCornerCrossingAllowedRatio = new ToggleButton(
        'Allow Corner Crossing',
        680,
        340,
        isCrossCornersAllowed,
        toggleCornerCrossing
    )
    manhattanHeuristicSelectButton = new SelectButton(
        undefined,
        'Manhattan Heuristic',
        680,
        405,
        true,
        undefined
    )
    octileHeuristicSelectButton = new SelectButton(
        DiagonalHeuristicsSelectOptions,
        'Octile Heuristic',
        680,
        405,
        heuristicType === 'octile',
        setOctileHeuristic
    )
    euclideanHeuristicSelectButton = new SelectButton(
        DiagonalHeuristicsSelectOptions,
        'Euclidean Heuristic',
        680,
        435,
        heuristicType === 'euclidean',
        setEuclideanHeuristic
    )
    chebyshevHeuristicSelectButton = new SelectButton(
        DiagonalHeuristicsSelectOptions,
        'Chebyshev Heuristic',
        680,
        465,
        heuristicType === 'chebyshev',
        setChebyshevHeuristic
    )

    GUIElements.push(pauseStartButton)
    GUIElements.push(stepButton)
    GUIElements.push(clearButton)
    GUIElements.push(restartButton)

    GUIElements.push(isDiagonalAllowedRadio)
    GUIElements.push(isBidirectionalRadio)
    GUIElements.push(isCornerCrossingAllowedRatio)

    GUIElements.push(dijkstraSelectButton)
    GUIElements.push(aStarSelectButton)
    GUIElements.push(bestFirstSelectButton)
    GUIElements.push(breadthFirstSelectButton)
    GUIElements.push(depthirstSelectButton)

    // GUIElements.push(octileHeuristicSelectButton)
    // GUIElements.push(euclideanHeuristicSelectButton)
    // GUIElements.push(chebyshevHeuristicSelectButton)

    AlgorithmSelectOptions.push(dijkstraSelectButton)
    AlgorithmSelectOptions.push(aStarSelectButton)
    AlgorithmSelectOptions.push(bestFirstSelectButton)
    AlgorithmSelectOptions.push(breadthFirstSelectButton)
    AlgorithmSelectOptions.push(depthirstSelectButton)

    DiagonalHeuristicsSelectOptions.push(octileHeuristicSelectButton)
    DiagonalHeuristicsSelectOptions.push(euclideanHeuristicSelectButton)
    DiagonalHeuristicsSelectOptions.push(chebyshevHeuristicSelectButton)
}

const renderGUI = _ => {
    background(255)
    strokeWeight(1)
    stroke(0)
    noFill()
    rect(660, 70, 230, 175)
    rect(660, 255, 230, 115)
    fill(0)
    noStroke()
    text('Pathfinding algorithms', 665, 85)
    text('Global settings', 665, 270)
    for (let i = 0; i < GUIElements.length; i++) {
        GUIElements[i].show()
    }
    if (findAlgorithm === 'astar' || findAlgorithm === 'bestfirst') {
        strokeWeight(1)
        stroke(0)
        noFill()
        rect(660, 380, 230, 115)
        fill(0)
        noStroke()
        text('Algorithm settings', 665, 395)
        if (isDiagonalAllowed) {
            for (let i = 0; i < DiagonalHeuristicsSelectOptions.length; i++) {
                DiagonalHeuristicsSelectOptions[i].show()
            }
        } else {
            manhattanHeuristicSelectButton.show()
        }
    }
    fill(0)
    noStroke()
    text('Status: ' + stateLabels[state], 660, 20)
}
