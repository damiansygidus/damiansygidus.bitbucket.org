class Node {
    constructor(i, j, x, y, width, height, isObstacle, grid) {
        this.grid = grid

        this.i = i
        this.j = j
        this.x = x
        this.y = y
        this.width = width
        this.height = height

        this.f = 0
        this.g = 0
        this.h = 0
        this.vh = 0

        this.neighbors
        this.neighborObstacles

        this.previous = undefined

        this.isObstacle = isObstacle

        this.visited = false

        this.visitedBy

        this.show = color => {
            noStroke()
            if (this.isObstacle) {
                fill(0)
                noStroke()
                ellipse(
                    this.x + this.width * 0.5,
                    this.y + this.width * 0.5,
                    this.width * 0.5,
                    this.height * 0.5
                )

                stroke(0)
                strokeWeight(this.width / 2)

                let nObstacles = this.getNeighborObstacles(this.grid)
                for (let i = 0; i < nObstacles.length; i++) {
                    let no = nObstacles[i]

                    if (
                        (no.i > this.i && no.j == this.j) ||
                        (no.i == this.i && no.j > this.j)
                    ) {
                        line(
                            this.x + this.width / 2,
                            this.y + this.height / 2,
                            no.x + no.width / 2,
                            no.y + no.height / 2
                        )
                    }

                    if (
                        !isCrossCornersAllowed &&
                        no.j > this.j &&
                        (no.i < this.i || no.i > this.i)
                    ) {
                        line(
                            this.x + this.width / 2,
                            this.y + this.height / 2,
                            no.x + no.width / 2,
                            no.y + no.height / 2
                        )
                    }
                }
            } else if (color) {
                fill(color)
                noStroke()
                rect(this.x, this.y, this.width, this.height)
            }
        }

        this.getNeighbors = _ => {
            if (!this.neighbors) {
                this.findNeighbors()
            }
            return this.neighbors
        }

        this.getNeighborObstacles = _ => {
            if (!this.neighborObstacles) {
                this.findNeighbors()
            }
            return this.neighborObstacles
        }

        const CrossMoves = [
            [0, -1], // Up
            [1, 0], // Right
            [0, 1], // Down
            [-1, 0] // Left
        ]
        const DiagonalMoves = [
            [1, -1], 
            [1, 1], 
            [-1, 1],
            [-1, -1],
        ]
        const DiagonalBlockers = [
            [0, 1], 
            [1, 2], 
            [2, 3], 
            [3, 0]
        ]

        this.getNode = (i, j) => {
            if (
                i < 0 ||
                i >= this.grid.length ||
                j < 0 ||
                j >= this.grid[0].length
            ) {
                return null
            }
            return this.grid[i][j]
        }

        this.findNeighbors = _ => {
            this.neighbors = []
            this.neighborObstacles = []

            for (let i = 0; i < 4; i++) {
                let node = this.getNode(
                    this.i + CrossMoves[i][0],
                    this.j + CrossMoves[i][1]
                )
                if (node != null) {
                    if (!node.isObstacle) {
                        this.neighbors.push(node)
                    } else {
                        this.neighborObstacles.push(node)
                    }
                }
            }

            for (let i = 0; i < 4; i++) {
                let gridX = this.i + DiagonalMoves[i][0]
                let gridY = this.j + DiagonalMoves[i][1]

                let node = this.getNode(gridX, gridY)

                if (node != null) {
                    if (isDiagonalAllowed && !node.isObstacle) {
                        if (!isCrossCornersAllowed) {
                            let border1 = DiagonalBlockers[i][0]
                            let border2 = DiagonalBlockers[i][1]

                            let blocker1 = this.grid[
                                this.i + CrossMoves[border1][0]
                            ][this.j + CrossMoves[border1][1]]
                            let blocker2 = this.grid[
                                this.i + CrossMoves[border2][0]
                            ][this.j + CrossMoves[border2][1]]

                            if (!blocker1.isObstacle || !blocker2.isObstacle) {
                                this.neighbors.push(node)
                            }
                        } else {
                            this.neighbors.push(node)
                        }
                    }
                    if (node.isObstacle) {
                        this.neighborObstacles.push(node)
                    }
                }
            }
        }
    }
}
