class SearchMap {
    constructor(columns, rows, x, y, w, h, obstacleRatio) {
        this.columns = columns
        this.rows = rows

        this.grid = []

        this.x = x
        this.y = y
        this.w = w
        this.h = h

        for (let i = 0; i < columns; i++) {
            this.grid[i] = []
        }

        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                let isObstacle = random(1.0) < obstacleRatio
                this.grid[i][j] = new Node(
                    i,
                    j,
                    x + (i * w) / columns,
                    y + (j * h) / rows,
                    w / columns,
                    h / rows,
                    isObstacle,
                    this.grid
                )
            }
        }
    }
}
