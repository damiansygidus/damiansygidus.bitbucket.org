const calculatePath = node => {
    startTimer()

    path = []
    let temp = node
    path.push(temp)
    while (temp.previous) {
        path.push(temp.previous)
        temp = temp.previous
    }
    recordTimer('Calculate Path')
    return path
}

const calculateBidirectionalPath = (startNode, goalNode) => {
    startTimer()

    path = []
    let startTemp = startNode
    let goalTemp = goalNode

    path.push(startTemp)
    while (startTemp.previous) {
        path.push(startTemp.previous)
        startTemp = startTemp.previous
    }

    path.unshift(goalTemp)
    while (goalTemp.previous) {
        path.unshift(goalTemp.previous)
        goalTemp = goalTemp.previous
    }
    recordTimer('Calculate Bidirectional Path')

    return path
}

const drawPath = path => {
    noFill()
    stroke(color('red'))
    strokeWeight(gamemap.w / gamemap.columns / 2)
    beginShape()
    for (let i = 0; i < path.length; i++) {
        vertex(path[i].x + path[i].width / 2, path[i].y + path[i].height / 2)
    }
    endShape()
}

const getSolutionPath = _ => {
    return !isBidirectional
        ? calculatePath(pathfinder.lastCheckedNode)
        : calculateBidirectionalPath(
              pathfinder.startLastCheckedNode,
              pathfinder.goalLastCheckedNode
          )
}

const calculateSolutionDistance = solutionPath => {
    let path = solutionPath
    let distance = 0

    for (let i = 1; i < path.length; i++) {
        distance +=
            path[i].i - path[i - 1].i === 0 || path[i].j - path[i - 1].j === 0 ? 1 : Math.SQRT2
    }

    return distance
}
