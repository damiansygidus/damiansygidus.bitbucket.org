poszukiwaniedrogi
==============
#### A pathfinding algorithms comparison tool written in JavaScript.

Basic Usage
-----------
To run the app you'll need a web browser with ECMAScript 6 support. To use it simply open `index.html` file.  
Basic params are set in configuration file located in `src/config.js`.  
To enable/disable automated tests toggle flag `pathfinderTestsMode` value.

You can play with it online: https://damiansygidus.bitbucket.io/

Debug mode
----------
If you want to run the program in debug mode you need to install the following apps:
- [Google Chrome](https://www.google.com/chrome/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [http-server](https://www.npmjs.com/package/http-server)
- [Debugger for Chrome](https://code.visualstudio.com/blogs/2016/02/23/introducing-chrome-debugger-for-vs-code)

License
-------
App created by Damian Sygidus. Use under [MIT License](http://www.opensource.org/licenses/mit-license.php).
